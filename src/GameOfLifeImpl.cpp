// GameOfLifeImpl.cpp
#include "GameOfLife.hpp"
#include <stdexcept>


namespace gol
{


class GameOfLife::GameOfLifeImpl
{
public:

  explicit
  GameOfLifeImpl(
                 std::vector< bool >            initState,
                 std::vector< bool >::size_type width
                 );

  void propogateState ( );

  std::vector< bool >::size_type getWidth( ) { return width_; }
  std::vector< bool >::size_type getHeight( ) { return height_; }

  const std::vector< bool >& getState( ) const { return currState_; }


private:

  std::vector< bool > currState_;
  std::vector< bool > prevState_;
  std::vector< bool >::size_type width_, height_;
};



GameOfLife::GameOfLifeImpl::GameOfLifeImpl(
                                           std::vector< bool >            initState,
                                           std::vector< bool >::size_type width
                                           )
  : currState_( initState )
  , prevState_( currState_.size( ) )
  , width_( width )
  , height_( initState.size( ) / width_ )
{
  if ( initState.size( ) % width_ != 0 )
  {
    throw std::runtime_error( "initialState vector not evenly divisible by width" );
  }
}



void
GameOfLife::GameOfLifeImpl::propogateState( )
{
  prevState_ = currState_;

  int iw = static_cast< int >( width_ );
  int ih = static_cast< int >( height_ );

  for ( decltype( height_ )y = 0; y < height_; ++y )
  {
    for ( decltype( width_ )x = 0; x < width_; ++x )
    {
      int neighbors = 0;

      for ( int yy = -1; yy <= 1; ++yy )
      {
        for ( int xx = -1; xx <= 1; ++xx )
        {
          if ( xx == 0 && yy == 0 )
          {
            continue;
          }

          auto ex =
            static_cast< decltype( width_ ) >( ( static_cast< int >( x ) + iw + xx ) % iw );
          auto why =
            static_cast< decltype( width_ ) >( ( static_cast< int >( y ) + ih + yy ) % ih );

          if ( prevState_[ why * width_ + ex ] )
          {
            ++neighbors;
          }
        }
      }

      auto index = y * width_ + x;
      auto state = prevState_[ index ];

      if ( state && ( neighbors != 2 && neighbors != 3 ) )
      {
        currState_[ index ] = false;
      }
      else
      if ( !state && neighbors == 3 )
      {
        currState_[ index ] = true;
      }
      else
      {
        currState_[ index ] = state;
      }

    }
  }
} // GameOfLife::GameOfLifeImpl::propogateState



} // namespace gol

#include "GameOfLife.cpp"
