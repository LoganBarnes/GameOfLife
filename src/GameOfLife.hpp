// GameOfLife.hpp
#pragma once

#include <vector>
#include <memory>
#include <iostream>

namespace gol
{

class GameOfLife
{
public:

  explicit
  GameOfLife(
             std::vector< bool >            initState,
             std::vector< bool >::size_type width
             );

  ~GameOfLife( );

  void propogateState ( );

  std::vector< bool >::size_type getWidth ( );
  std::vector< bool >::size_type getHeight ( );

  const std::vector< bool > &getState ( );

  // default move constructor/operator
  GameOfLife( GameOfLife&& )           = default;
  GameOfLife&operator=( GameOfLife&& ) = default;

  // no default constructor or copy constructor/operators
  GameOfLife( ) = delete;

  GameOfLife( GameOfLife& )           = delete;
  GameOfLife&operator=( GameOfLife& ) = delete;

  friend
  std::ostream&operator<<(
                          std::ostream     &os,
                          const GameOfLife &g
                          );


private:

  void _printState ( std::ostream &os ) const;

  class GameOfLifeImpl;
  std::unique_ptr< GameOfLifeImpl > upImpl_;
};

} // namespace gol
